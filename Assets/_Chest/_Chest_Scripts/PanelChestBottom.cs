﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelChestBottom : MonoBehaviour
{
    public GameObject TapForInformationObject;
    public Button NoThanksButton;
    public Button RewardedButton;
    public PanelKey KeyPanel;
    public GameObject TapToContinue;

    private ChestPanel m_ChestPanel;

    public void Initialize(ChestPanel chestPanel)
    {
        m_ChestPanel = chestPanel;
        KeyPanel.Initialize(this);

    }

    public void AfterWatchRewardedVideo()
    {
        NoThanksButton.gameObject.SetActive(false);
        RewardedButton.gameObject.SetActive(false);
        TapForInformationObject.SetActive(true);
        KeyPanel.Rewarded();
    }

    public void Open()
    {
        TapToContinue.SetActive(false);
        NoThanksButton.gameObject.SetActive(false);
        RewardedButton.gameObject.SetActive(false);
        TapForInformationObject.SetActive(true);
    }

    public void KeysUsed()
    {
        gameObject.SetActive(true);

        RewardedButton.onClick.RemoveAllListeners();
        RewardedButton.onClick.AddListener(() => m_ChestPanel.ShowRewardedVideo(AfterWatchRewardedVideo));

        NoThanksButton.onClick.RemoveAllListeners();
        NoThanksButton.onClick.AddListener(() => m_ChestPanel.Close());

        NoThanksButton.gameObject.SetActive(true);
        RewardedButton.gameObject.SetActive(true);
        TapForInformationObject.SetActive(false);
    }

    public void FinishProcess()
    {
        TapToContinue.SetActive(true);
        m_ChestPanel.PanelExitObject.SetActive(true);
        TapForInformationObject.SetActive(false);
    }
}
