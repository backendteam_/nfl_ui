﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestItemUI : MonoBehaviour
{
    public Animator Animator;
    public GameObject BeforePanel;
    public Button Button;
    public Image ChestImage;


    public GameObject AfterPanel;
    public Image RewardImage;
    public TextMeshProUGUI RewardText;

    private ChestItemUIData m_Data;
    private IChestRewarded m_ChestRewarded;
    private IChestKey m_ChestKey;

    public void Initialize(ChestItemUIData chestItemUIData, IChestRewarded chestRewarded, IChestKey chestKey, Sprite defaultChestSprite)
    {
        m_ChestRewarded = chestRewarded;
        m_ChestKey = chestKey;
        m_Data = chestItemUIData;

        PrepareUI(defaultChestSprite);
    }


    private void PrepareUI(Sprite defaultChestSprite)
    { 
        BeforePanel.SetActive(true);
        AfterPanel.SetActive(false);
        Button.interactable = true;
        ChestImage.sprite = defaultChestSprite;
        RewardImage.sprite = m_Data.Sprite;
        RewardText.text = m_Data.Value.ToString();
        Button.onClick.RemoveAllListeners();
        Button.onClick.AddListener(() => OnButtonClicked());
    }

    public void OnButtonClicked()
    {
        Debug.Log("Clicked");
        if (m_ChestKey.HasKey())
        {
            Animator.SetTrigger("open"); //When finish AnimationEnd action will be called!
            Button.interactable = false;
        }
    }

    public void AnimationEnd()
    {
        m_ChestKey.KeyUsed();
        BeforePanel.SetActive(false);
        AfterPanel.SetActive(true);
        switch (m_Data.ChestType)
        {
            case ChestType.Gold:
                GoldReward(m_Data.Value);
                break;
            case ChestType.Item:
                ItemReward(m_Data.Value, m_Data.Id, m_Data.Sprite);
                break;
        }
    }


    private void GoldReward(int value)
    {
        RewardText.gameObject.SetActive(true);
        m_ChestRewarded.ChestRewarded(ChestType.Gold, value, null);

        RewardImage.rectTransform.anchorMin = new Vector2(.5f, 1);
        RewardImage.rectTransform.anchorMax = new Vector2(.5f, 1);
        RewardImage.rectTransform.anchoredPosition = new Vector3(0, -15, 0);
        RewardImage.rectTransform.sizeDelta = new Vector2(131, 121);
    }

    private void ItemReward(int value, string id, Sprite sprite)
    {
        m_ChestRewarded.ChestRewarded(ChestType.Item, value, id);
        RewardText.gameObject.SetActive(false);

        RewardImage.sprite = sprite;

        RewardImage.rectTransform.anchorMin = Vector2.zero;
        RewardImage.rectTransform.anchorMax = Vector2.one;
        RewardImage.rectTransform.offsetMax = new Vector2(-4.78f, -6.32f);
        RewardImage.rectTransform.offsetMin = new Vector2(5.425f, 5.84f);
    }
}
