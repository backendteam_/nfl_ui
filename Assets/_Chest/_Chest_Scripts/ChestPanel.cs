﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ChestPanel : MonoBehaviour, IChestPanel, IChestRewarded
{
    public Image BestItemImage;
    public Sprite GoldSprite;
    public Sprite DefaultChestSprite;
    public PanelKey KeyPanel;
    public PanelChestBottom ChestBottomPanel;
    public GameObject PanelExitObject;

    private List<ChestItemUI> _chestItems;

    public void Initialize()
    {
        _chestItems = GetComponentsInChildren<ChestItemUI>().ToList();
        ChestBottomPanel.Initialize(this);
    }

    public void Open(ChestItemUIData itemUIData) 
    {
        gameObject.SetActive(true);
        KeyPanel.Open();
        ChestBottomPanel.Open();
        PanelExitObject.SetActive(false);
        SetItemProperties(itemUIData.Sprite);
        var chests  = new ChestGoldSystem(new ChestGoldParameter())
            .FillChest(GoldSprite);

        var index = UnityEngine.Random.Range(0, chests.Count);
        chests.Insert(index, itemUIData);

        for (int i = 0; i < chests.Count; i++)
        {
            var chestItem = chests[i];
            var chestUIItem = _chestItems[i];

            chestUIItem.Initialize(chestItem, this, KeyPanel, DefaultChestSprite);
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    private void SetItemProperties(Sprite sprite)
    {
        BestItemImage.sprite = sprite;
    }

    public void ShowRewardedVideo(Action doAfterWatchedIfSuccess)
    {
        Debug.Log("Call rewarded video here !!!");
        doAfterWatchedIfSuccess();
    }

    #region Interface-Event

    //When user earn reward. This method will be called
    public void ChestRewarded(ChestType chestType, int value, string key)
    {
        Debug.Log($"User earn : {value} - Type is : {chestType}");
    }

    #endregion
}
