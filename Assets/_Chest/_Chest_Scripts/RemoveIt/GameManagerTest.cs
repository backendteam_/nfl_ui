﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTest : MonoBehaviour
{
    public Sprite ItemSprite;
    public ChestPanel ChestPanel;
    public void Awake()
    {
        ChestPanel.Initialize();
    }
    public void Start()
    {

    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ChestItemUIData data = new ChestItemUIData("BALL", 999, ItemSprite, ChestType.Item);
            ChestPanel.Open(data);
        }
    }
}
