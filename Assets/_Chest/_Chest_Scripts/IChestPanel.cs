﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IChestPanel
{
    void Open(ChestItemUIData itemUIData);
}
