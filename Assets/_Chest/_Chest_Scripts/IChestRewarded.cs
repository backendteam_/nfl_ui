﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IChestRewarded
{

    void ChestRewarded(ChestType chestType, int value, string id);
}
