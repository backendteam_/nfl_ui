﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ChestItemUIData
{
    public string Id { get; set; } //ItemId
    public int Value { get; set; }
    public Sprite Sprite { get; set; }
    public ChestType ChestType { get; set; }

    public ChestItemUIData(string id, int value, Sprite sprite, ChestType chestType)
    {
        Id = id;
        Value = value;
        Sprite = sprite;
        ChestType = chestType;
    }
}
