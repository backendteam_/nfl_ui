﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestGoldParameter
{
	public readonly int CoEfficientValue = 5;
	public readonly int MinimumGoldValue = 5;  //User can earn minimum 100 golds per chest
	public readonly int MaximumGoldValue = 50;  //User can earn maximum 200 golds per chest
	public readonly int TotalEarnableMinGoldValuePerSystem = 60; // User never earn golds less than 1000 in total;
	public readonly int TotalEarnableMaxGoldValuePerSystem = 250; // User never earn golds more than 1400 in total;

	public ChestGoldParameter()
	{ 
	
	}

	public ChestGoldParameter(int coEfficientValue, int minimumGoldValue, int maximumGoldValue, int totalEarnableMaxGoldValue, int totalEarnableMinGoldValue)
	{
		CoEfficientValue = coEfficientValue;
		MinimumGoldValue = minimumGoldValue;
		MaximumGoldValue = maximumGoldValue;
		TotalEarnableMinGoldValuePerSystem = totalEarnableMinGoldValue;
		TotalEarnableMaxGoldValuePerSystem = totalEarnableMaxGoldValue;
	}
}
