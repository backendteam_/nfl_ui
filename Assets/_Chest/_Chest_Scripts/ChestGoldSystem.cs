﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChestGoldSystem
{
	private readonly int m_TotalChestCount = 9;
	public ChestGoldParameter ChestGoldParameter { get; set; }

	private readonly List<int> m_Golds;
	private Sprite m_GoldSprite;

	public ChestGoldSystem(ChestGoldParameter chestGoldParameter)
	{
		ChestGoldParameter = chestGoldParameter;
		var startValue = chestGoldParameter.MinimumGoldValue / chestGoldParameter.CoEfficientValue;
		var endValue = (chestGoldParameter.MaximumGoldValue - chestGoldParameter.MinimumGoldValue) / chestGoldParameter.CoEfficientValue;

		m_Golds = Enumerable.Range(startValue, endValue).ToList();
	}


	public List<ChestItemUIData> FillChest(Sprite goldSprite)
	{
		m_GoldSprite = goldSprite;
		List<ChestItemUIData> returnableChestList = new List<ChestItemUIData>();

		var iterationCount = m_TotalChestCount - 1; // 1 item 8 golds
		for (int i = 0; i < iterationCount; i++)
		{
			var gold = m_Golds.ElementAt(Random.Range(0, m_Golds.Count)) * ChestGoldParameter.CoEfficientValue;
			ChestItemUIData item = CreateChestItem(gold);
			returnableChestList.Add(item);
		}

		CheckGoldLimits(returnableChestList);
		Debug.Log(returnableChestList.Sum(x => x.Value).ToString());
		return returnableChestList;
	}

	private ChestItemUIData CreateChestItem(int gold)
	{
		ChestItemUIData item = new ChestItemUIData(null, gold, m_GoldSprite, ChestType.Gold);
		return item;
	}

	private void CheckGoldLimits(List<ChestItemUIData> returnableChestList)
	{
		int whileIterationTotalCount = 10;
		int whileIterationCurrentCount = 0;
		while (true)
		{
			var currentGoldValue = returnableChestList.Sum(x => x.Value);
			if (currentGoldValue > ChestGoldParameter.TotalEarnableMinGoldValuePerSystem && currentGoldValue < ChestGoldParameter.TotalEarnableMaxGoldValuePerSystem)
				break;

			if (currentGoldValue > ChestGoldParameter.TotalEarnableMaxGoldValuePerSystem)
				ChangeItemWithLowestOne(returnableChestList);
			else if (currentGoldValue < ChestGoldParameter.TotalEarnableMinGoldValuePerSystem)
				ChangeItemWithHighestOne(returnableChestList);
			
			if (whileIterationCurrentCount++ > whileIterationTotalCount)
				break;
		}
	}

	private void ChangeItemWithHighestOne(List<ChestItemUIData> returnableChestList)
	{
		var findLowestOne = returnableChestList.OrderBy(x => x.Value).FirstOrDefault();
		var lowValue = findLowestOne.Value / ChestGoldParameter.CoEfficientValue;
		returnableChestList.Remove(findLowestOne);

		var query = m_Golds.Where(x => x > lowValue);
		var gold = query.ElementAt(Random.Range(0, query.Count())) * ChestGoldParameter.CoEfficientValue;
		var chestItem = CreateChestItem(gold);
		returnableChestList.Add(chestItem);
	}

	private void ChangeItemWithLowestOne(List<ChestItemUIData> returnableChestList)
	{
		var findHigherOne = returnableChestList.OrderByDescending(x => x.Value).FirstOrDefault();
		var highValue = findHigherOne.Value / ChestGoldParameter.CoEfficientValue;
		returnableChestList.Remove(findHigherOne);

		var query = m_Golds.Where(x => x < highValue);
		var gold = query.ElementAt(Random.Range(0, query.Count())) * ChestGoldParameter.CoEfficientValue;
		var chestItem = CreateChestItem(gold);
		returnableChestList.Add(chestItem);
	}
}
