﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelKey : MonoBehaviour, IChestKey
{
    public List<Image> KeyImages;
    public GameObject DoubleKeyObject;
    public Sprite KeyFillSprite;
    public Sprite KeyEmptySprite;

    public PanelChestBottom ChestBottomPanel { get; set; }

    private int m_UsedKeyCount = 0;
    private int m_ChestKeyLimit = 3;
    private int m_SecondPartLimit = 6;
    private int m_TotalChestCount = 9;
    private bool m_Rewarded;

    public void Initialize(PanelChestBottom chestBottomPanel)
    {
        ChestBottomPanel = chestBottomPanel;
    }

    public void Open()
    {
        m_UsedKeyCount = 0;
        m_Rewarded = false;
        gameObject.SetActive(true);
        DoubleKeyObject.SetActive(false);
        SetStateKeyImages();
    }

    public void Close()
    { 
        gameObject.SetActive(false);
    }

    public void Rewarded()
    {
        DoubleKeyObject.SetActive(true);
        m_Rewarded = true;
        SetStateKeyImages();
    }

    public void KeyUsed()
    {
        KeyImages[m_UsedKeyCount++ % m_ChestKeyLimit].sprite = KeyEmptySprite;
        if (m_UsedKeyCount == m_TotalChestCount)
        {
            ChestBottomPanel.FinishProcess();
        }
        else if (m_UsedKeyCount == m_SecondPartLimit)
        {
            DoubleKeyObject.SetActive(false);
            SetStateKeyImages();
        }
        else if (m_UsedKeyCount == m_ChestKeyLimit)
        { 
            ChestBottomPanel.KeysUsed();
        }
    }

    public bool HasKey()
        => m_UsedKeyCount < m_ChestKeyLimit || m_Rewarded;

    public void SetStateKeyImages()
    { 
        KeyImages.ForEach(x => x.sprite = KeyFillSprite);
    }

}
