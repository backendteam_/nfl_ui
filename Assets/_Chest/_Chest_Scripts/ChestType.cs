﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChestType
{
    Gold = 0,
    Item = 1
}
