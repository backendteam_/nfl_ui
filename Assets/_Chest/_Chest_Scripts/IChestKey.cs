﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IChestKey
{
    bool HasKey();
    void Rewarded();
    void KeyUsed();
}
